#!/usr/bin/env node

var avoid = require("../libavoid_js")
console.log("libavoid-js version: "+avoid.version());

var router = avoid.Router();
var conn = router.Connector({x: 1.2, y: 0.5, dir: avoid.Left}, {x: 1.5, y:4, dir: avoid.Up});
var conn2 = router.Connector([1.2,0.5], [1.5,4]);
var rect1 = router.Rectangle({x:0, y:10}, [10,0]);
var rect2 = router.Rectangle([100,10], {x:110,y:0});
rect2.morph([[0,0],[5,0],[5,5],[0,5]]);

var router2 = avoid.Router(avoid.OrthogonalRouting);
var rect_xxx = router2.Rectangle({x:0, y:10}, [10,0]);
console.log("IDs:", conn.id(), conn2.id(), rect1.id(), rect2.id(),
            rect_xxx.id());

rect2 = router.Polygon([[10, 10], [100,10], [100, 100], [10, 100]]);
rect1.Pin("center");
rect2.Pin("left");
var conn3 = router.Connector({shape: rect1, pins: "center"},
                             {shape: rect2, pins: "left"});
console.log("route", conn3.displayRoute());
conn3.setCallback(function CB() {
    console.log("CALLBACK! with", this.displayRoute());
});

conn.setSourceEndpoint({x: 1.2, y: 0.5, dir: avoid.Left});
conn2.setDestEndpoint([20, 4]);

rect2.morph([[0, 0], [10,0], [10, 10], [0, 10]]);
rect2.move({x:400, y:400});

router.processTransaction();
router.Polygon([{x:0, y:0}, {x:1, y:0}, {x: 0, y: 1}]);

console.log("needsRepaint:", conn3.needsRepaint());
