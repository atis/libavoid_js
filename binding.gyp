# Please make sure this file uses Unix-style line endings (\n).
{
    "targets": [
        {
            "target_name": "libavoid_js",
            "dependencies": [
                "libavoid/binding.gyp:libavoid"
            ],
            "sources": [
                "src/libavoid_js.cpp",
                "src/log.c"
            ],
            "include_dirs": [
                "libavoid"
            ],
            "defines": ["NDEBUG"]
        }
    ]
}
