#ifndef JSAVOID_CONNREF_H
#define JSAVOID_CONNREF_H

/* JS reference destructor (used as argument to Pointer::Pointer()). */
static void avoid_ConnRef_destruct(void *p)
{
    assert(p != NULL);
    ((ConnRef *)p)->jsref = NULL;
}

/* Create Connector. */
static Handle<Value> avoid_ConnRef_new(const Arguments& args)
{
    HandleScope scope;
    JS_ARG_RANGE(3, 3);

    /* Get Router address. */
    JS_ARGCHECK_OBJECT(0);
    PTR_EXTRACT(Router, router, args[0]->ToObject(), PTR_ROUTER);

    /* Create a connector without endpoints. */
    ConnRef *conn = new ConnRef(router);
    
    /* Set source endpoint. */
    JS_ASSERT(args[1]->IsObject(), "Unexpected source endpoint argument.");
    Local<Object> src = args[1]->ToObject();
    if (src->Get(String::NewSymbol("shape"))->IsObject()) {
        /* Set pinned source endpoint. */
        ShapeRef *shape;
        unsigned pinclass_id;
        JS_EXTRACT_PINNED_CONNEND(src, shape, pinclass_id);
        conn->setSourceEndpoint(ConnEnd(shape, pinclass_id));
    } else {
        /* Set free-floating source endpoint. */
        double x, y;
        JS_EXTRACT_VECTOR(src, x, y);
        unsigned dir = src->Get(String::NewSymbol("dir"))->Int32Value();
        conn->setSourceEndpoint(ConnEnd(Point(x, y), dir));
    }

    /* Set destination endpoint. */
    JS_ASSERT(args[2]->IsObject(), "Unexpected dest endpoint argument.");
    Local<Object> dst = args[2]->ToObject();
    if (dst->Get(String::NewSymbol("shape"))->IsObject()) {
        /* Set pinned source endpoint. */
        ShapeRef *shape;
        unsigned pinclass_id;
        JS_EXTRACT_PINNED_CONNEND(dst, shape, pinclass_id);
        conn->setDestEndpoint(ConnEnd(shape, pinclass_id));
    } else {
        /* Set free-floating source endpoint. */
        double x, y;
        JS_EXTRACT_VECTOR(dst, x, y);
        unsigned dir = dst->Get(String::NewSymbol("dir"))->Int32Value();
        conn->setDestEndpoint(ConnEnd(Point(x, y), dir));
    }

    conn->jsref = PTR_WRAP(args.This(), conn, PTR_CONNREF,
                           avoid_ConnRef_destruct);
    log_msg("Created ConnRef %p (id=%d router=%p jsref=%p)", conn, conn->id(),
            router, conn->jsref);
    return scope.Close(args.This());
}

/*
 * Connector.id() -> IntegerID
 *
 * Return connector ID (numeric).
 */
static Handle<Value> avoid_ConnRef_id(const Arguments& args)
{
    HandleScope scope;
    JS_ARG_RANGE(0, 0);

    PTR_EXTRACT(ConnRef, conn, args.This(), PTR_CONNREF);
    return scope.Close(Integer::NewFromUnsigned(conn->id()));
}

/*
 * Connector.setSourceEndpoint(endpoint) -> this
 *
 * Set source endpoint. The argument must have the same form as the source
 * argument for connector constructor: Router.Connector(). A quick reference:
 *      {x:xpos, y:ypos} or
 *      {x:xpos, y:ypos, dir:dirFlags} or
 *      [xpos, ypos] or
 *      {shape:Shape, pins:pinClassName}
 */
static Handle<Value> avoid_ConnRef_setSourceEndpoint(const Arguments& args)
{
    HandleScope scope;
    JS_ARG_RANGE(1, 1);

    /* Extract ConnRef pointer from "this". */
    PTR_EXTRACT(ConnRef, conn, args.This(), PTR_CONNREF);

    /* Set endpoint. */
    JS_ASSERT(args[0]->IsObject(), "Unexpected endpoint argument.");
    Local<Object> endpoint = args[0]->ToObject();
    if (endpoint->Get(String::NewSymbol("shape"))->IsObject()) {
        /* Set pinned source endpoint. */
        ShapeRef *shape;
        unsigned pinclass_id;
        JS_EXTRACT_PINNED_CONNEND(endpoint, shape, pinclass_id);
        conn->setSourceEndpoint(ConnEnd(shape, pinclass_id));
    } else {
        /* Set free-floating source endpoint. */
        double x, y;
        JS_EXTRACT_VECTOR(endpoint, x, y);
        unsigned dir = endpoint->Get(String::NewSymbol("dir"))->Int32Value();
        conn->setSourceEndpoint(ConnEnd(Point(x, y), dir));
    }

    return scope.Close(args.This());
}

/*
 * Connector.setDestEndpoint(endpoint) -> this
 *
 * Set dest endpoint. The argument must have the same form as the destination
 * argument for connector constructor: Router.Connector(). A quick reference:
 *      {x:xpos, y:ypos} or
 *      {x:xpos, y:ypos, dir:dirFlags} or
 *      [xpos, ypos] or
 *      {shape:Shape, pins:pinClassName}
 */
static Handle<Value> avoid_ConnRef_setDestEndpoint(const Arguments& args)
{
    HandleScope scope;
    JS_ARG_RANGE(1, 1);

    /* Extract ConnRef pointer from "this". */
    PTR_EXTRACT(ConnRef, conn, args.This(), PTR_CONNREF);

    /* Set endpoint. */
    JS_ASSERT(args[0]->IsObject(), "Unexpected endpoint argument.");
    Local<Object> endpoint = args[0]->ToObject();
    if (endpoint->Get(String::NewSymbol("shape"))->IsObject()) {
        /* Set pinned source endpoint. */
        ShapeRef *shape;
        unsigned pinclass_id;
        JS_EXTRACT_PINNED_CONNEND(endpoint, shape, pinclass_id);
        conn->setDestEndpoint(ConnEnd(shape, pinclass_id));
    } else {
        /* Set free-floating source endpoint. */
        double x, y;
        JS_EXTRACT_VECTOR(endpoint, x, y);
        unsigned dir = endpoint->Get(String::NewSymbol("dir"))->Int32Value();
        conn->setDestEndpoint(ConnEnd(Point(x, y), dir));
    }

    return scope.Close(args.This());
}

/*
 * Connector.setCheckpoints(arrayOfPoints) -> this
 *
 * Allows user to specify a set of checkpoints that this connector will route
 * via.
 */
static Handle<Value> avoid_ConnRef_setCheckpoints(const Arguments& args)
{
    HandleScope scope;
    JS_ARG_RANGE(1, 1);
    JS_ARGCHECK_ARRAY(0);

    /* Array of points given by user. */
    Local<Array> points = Local<Array>::Cast(args[0]);
    unsigned n = points->Length();

    /* Iterate over supplied points and add them to checkpoint list. */
    std::vector<Checkpoint> cp;
    for (unsigned i = 0; i < n; i++) {
        double x, y;
        JS_EXTRACT_VECTOR(points->Get(i), x, y);
        cp.push_back(Checkpoint(Point(x, y)));
    }

    /* Call setRoutingCheckpoints() on connector instance. */
    PTR_EXTRACT(ConnRef, conn, args.This(), PTR_CONNREF);
    conn->setRoutingCheckpoints(cp);

    return scope.Close(args.This());
}

/* Called by libavoid whenever a connector needs to be redrawn. */
static void connref_callback(void *data)
{
    HandleScope scope;

    /* Get Connector Pointer. */
    Pointer *c_ptr = (Pointer *)data;
    assert(c_ptr->type_id == PTR_CONNREF);

    /* Execute callback. */
    c_ptr->pf_handle->Call(c_ptr->self, 0, NULL);
}

/*
 * Connector.setCallback(fn) -> this
 *
 * Register a function to be called when connector needs to be redrawn.
 */
static Handle<Value> avoid_ConnRef_setCallback(const Arguments& args)
{
    HandleScope scope;
    JS_ARG_RANGE(1, 1);
    JS_ARGCHECK_FUNCTION(0);

    /* Store user callback + self inside Pointer struct. */
    PTR_EXTRACT(ConnRef, conn, args.This(), PTR_CONNREF);
    ptr_conn->self = Persistent<Object>::New(args.This());
    ptr_conn->pf_handle = Persistent<Function>::New(Handle<Function>::Cast(args[0]));
    
    /* Set callback. */
    conn->setCallback(connref_callback, ptr_conn);
    return scope.Close(args.This());
}

/*
 * Connector.needsRepaint() -> true/false
 *
 * Returns an indication of whether this connector has a new route and thus
 * needs to be repainted. If the connector has been rerouted and needs
 * repainting, the displayRoute() method can be called to get a reference to the
 * new route.
 */
static Handle<Value> avoid_ConnRef_needsRepaint(const Arguments& args)
{
    HandleScope scope;
    JS_ARG_RANGE(0, 0);

    /* Return needsRepaint() boolean value. */
    PTR_EXTRACT(ConnRef, conn, args.This(), PTR_CONNREF);
    return scope.Close(Boolean::New(conn->needsRepaint()));
}

/*
 * Connector.displayRoute(arrayForm=false) -> arrayOfPoints
 *
 * Return current display version of route. If "arrayForm" argument has a truthy
 * value, then the returned vectors are in array-form.
 */
static Handle<Value> avoid_ConnRef_displayRoute(const Arguments& args)
{
    HandleScope scope;
    JS_ARG_RANGE(0, 1);
    JS_DEFARG_BOOL(0, array_form, false);

    /* Get display route reference. */
    PTR_EXTRACT(ConnRef, conn, args.This(), PTR_CONNREF);
    const PolyLine &route = conn->displayRoute();

    /* Add points from route to a new array. */
    size_t sz = route.size();
    Local<Array> ptlist = Array::New(sz);
    if (array_form) {
        for (size_t i = 0; i < sz; i++) {
            const Point &pt = route.at(i);
            Local<Array> newpt = Array::New(2);
            newpt->Set(0, Number::New(pt.x));
            newpt->Set(1, Number::New(pt.y));
            ptlist->Set(i, newpt);
        }
    } else {
        for (size_t i = 0; i < sz; i++) {
            const Point &pt = route.at(i);
            Local<Object> newpt = Object::New();
            newpt->Set(String::NewSymbol("x"), Number::New(pt.x));
            newpt->Set(String::NewSymbol("y"), Number::New(pt.y));
            ptlist->Set(i, newpt);
        }
    }
    return scope.Close(ptlist);
}

/* Javascript function that creates new ConnRef instances. */
static Persistent<Function> avoid_ConnRef_factory;

/* Object setup: create factory function. */
static void avoid_ConnRef_setup(void)
{
    Local<FunctionTemplate> tpl = ptr_template("ConnRef", avoid_ConnRef_new);

    /* Add methods. */
    Local<ObjectTemplate> ptt = tpl->PrototypeTemplate();
    JS_SET_METHOD(ptt, "id", avoid_ConnRef_id);
    JS_SET_METHOD(ptt, "setCallback", avoid_ConnRef_setCallback);
    JS_SET_METHOD(ptt, "needsRepaint", avoid_ConnRef_needsRepaint);
    JS_SET_METHOD(ptt, "displayRoute", avoid_ConnRef_displayRoute);
    JS_SET_METHOD(ptt, "setSourceEndpoint", avoid_ConnRef_setSourceEndpoint);
    JS_SET_METHOD(ptt, "setDestEndpoint", avoid_ConnRef_setDestEndpoint);
    JS_SET_METHOD(ptt, "setCheckpoints", avoid_ConnRef_setCheckpoints);

    avoid_ConnRef_factory = Persistent<Function>::New(tpl->GetFunction());
}

#endif /* JSAVOID_CONNREF_H */
