#ifndef JSAVOID_ROUTER_H
#define JSAVOID_ROUTER_H

/* JS reference destructor (argument to Pointer::Pointer()). */
static void avoid_Router_destruct(void *p)
{
    /*
     * Warning: libavoid quirk. If new shapes have been added, then before they
     * may be deleted, the current transaction must be processed.
     */
    Router *router = (Router *)p;
    log_msg("Deleting Router instance %p (jsref=%p)", router, router->jsref);
    router->processTransaction();
    delete router;
}

/* Create Router. */
static Handle<Value> avoid_Router_new(const Arguments& args)
{
    HandleScope scope;
    JS_ARG_RANGE(0, 1);

    /* Create Router, wrap it, store as "this". */
    JS_DEFARG_INT32(0, flags, PolyLineRouting);
    Router *router = new Router(flags);
    router->jsref = PTR_WRAP(args.This(), router, PTR_ROUTER,
                             avoid_Router_destruct);
    log_msg("Created Router %p (jsref=%p)", router, router->jsref);
    return scope.Close(args.This());
}

/*
 * Router.Rectangle(topLeft, bottomRight) -> ShapeRef
 * XXX (not implemented) Router.Rectangle(centre, width, height) -> ShapeRef
 *
 * Create Rectangle that's bound to the Router. Return its wrapped ShapeRef
 * pointer.
 */
static Handle<Value> avoid_Router_Rectangle(const Arguments& args)
{
    HandleScope scope;
    JS_ARG_RANGE(2, 2);
    JS_ARGCHECK_OBJECT(0);
    JS_ARGCHECK_OBJECT(1);

    /* Extract top-left and bottom-right point coords. */
    double TL_x, TL_y, BR_x, BR_y;
    JS_EXTRACT_VECTOR(args[0], TL_x, TL_y);
    JS_EXTRACT_VECTOR(args[1], BR_x, BR_y);

    /* Create "external" handle with rectangle address. */
    Avoid::Rectangle rect(Point(TL_x, TL_y), Point(BR_x, BR_y));
    Local<External> ext_shape = External::New(&rect);

    /* Produce ShapeRef object by using its factory function. */
    Handle<Value> argv[2] = { args.This(), ext_shape };
    Local<Object> shape = avoid_ShapeRef_factory->NewInstance(2, argv);
    return scope.Close(shape);
}

/*
 * Router.Polygon([p1, p2, .., pN]) -> ShapeRef
 *
 * Create Polygon shape that's bound to the Router. Return its wrapped ShapeRef
 * pointer. The only argument is an array of points, each of which may have one
 * of the following forms: [xpos, ypos] or {x: xpos, y: ypos}.
 */
static Handle<Value> avoid_Router_Polygon(const Arguments& args)
{
    HandleScope scope;
    JS_ARG_RANGE(1, 1);
    JS_ARGCHECK_ARRAY(0);

    /* Create empty polygon. */
    Local<Array> points = Local<Array>::Cast(args[0]);
    unsigned n = points->Length();
    Polygon poly(n);

    /* Iterate over supplied points and add them to polygon. */
    for (unsigned i = 0; i < n; i++) {
        double x, y;
        JS_EXTRACT_VECTOR(points->Get(i), x, y);
        poly.setPoint(i, Point(x, y));
    }

    /* Create "external" handle with polygon address. */
    Local<External> ext_shape = External::New(&poly);

    /* Produce ShapeRef object by using its factory function. */
    Handle<Value> argv[2] = { args.This(), ext_shape };
    Local<Object> shape = avoid_ShapeRef_factory->NewInstance(2, argv);
    return scope.Close(shape);
}

/*
 * Router.Connector(source, destination) -> ConnRef
 *
 * Create a new connector. Both source and destination arguments may have one of
 * two forms. The first is free-floating vector form with optional direction
 * flags:
 *     {x:xpos, y:ypos} or {x:xpos, y:ypos, dir:dirFlags} or [xpos, ypos]
 *
 * The second form is the shape + pin_class (connector is pinned to shape) form:
 *     {shape: ShapeRef, pins: pinClassName}
 */
static Handle<Value> avoid_Router_ConnRef(const Arguments& args)
{
    HandleScope scope;
    JS_ARG_RANGE(2, 2);

    /* Produce connector. */
    Handle<Value> argv[3] = { args.This(), args[0], args[1] };
    Local<Object> conn = avoid_ConnRef_factory->NewInstance(3, argv);
    return scope.Close(conn);
}

/*
 * Router.processTransaction() -> this
 *
 * Causes the current transaction to be commited.
 */
static Handle<Value> avoid_Router_processTransaction(const Arguments& args)
{
    HandleScope scope;
    JS_ARG_RANGE(0, 0);

    /* Process transaction. */
    PTR_EXTRACT(Router, router, args.This(), PTR_ROUTER);
    router->processTransaction();
    return scope.Close(args.This());
}

/* Javascript function that creates new Router instances. */
static Persistent<Function> avoid_Router_factory;

/* Object setup: create factory function. */
static void avoid_Router_setup(void)
{
    Local<FunctionTemplate> tpl = ptr_template("Router", avoid_Router_new);

    /* Add methods. */
    Local<ObjectTemplate> ptt = tpl->PrototypeTemplate();
    JS_SET_METHOD(ptt, "Rectangle", avoid_Router_Rectangle);
    JS_SET_METHOD(ptt, "Polygon", avoid_Router_Polygon);
    JS_SET_METHOD(ptt, "Connector", avoid_Router_ConnRef);
    JS_SET_METHOD(ptt, "processTransaction", avoid_Router_processTransaction);

    avoid_Router_factory = Persistent<Function>::New(tpl->GetFunction());
}

/*
 * avoid.Router(routingFlags)
 *
 * User-callable function that creates and returns a Router object instance.
 * "routingFlags" may be set to PolyLineRouting (default) or OrthogonalRouting
 * or an ORed combination of both.
 */
static Handle<Value> avoid_Router(const Arguments& args)
{
    HandleScope scope;
    JS_ARG_RANGE(0, 1);
    
    /* Produce Router. */
    Handle<Value> argv[1] = { args[0] };
    Local<Object> instance = avoid_Router_factory->NewInstance(1, argv);
    return scope.Close(instance);
}

#endif /* JSAVOID_ROUTER_H */
