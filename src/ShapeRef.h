#ifndef JSAVOID_SHAPEREF_H
#define JSAVOID_SHAPEREF_H

/* JS reference destructor (used as argument to Pointer::Pointer()). */
static void avoid_ShapeRef_destruct(void *p)
{
    assert(p != NULL);
    ((ShapeRef *)p)->jsref = NULL;
}

/* Create ShapeRef. */
static Handle<Value> avoid_ShapeRef_new(const Arguments& args)
{
    HandleScope scope;
    JS_ARG_RANGE(2, 2);

    /* Get router address. */
    JS_ARGCHECK_OBJECT(0);
    PTR_EXTRACT(Router, router, args[0]->ToObject(), PTR_ROUTER);

    /* Polygon shape argument. */
    JS_ARGCHECK_EXTERNAL(1);
    Avoid::Polygon *poly = (Avoid::Polygon *)Local<External>::Cast(args[1])->Value();

    /* Create ShapeRef, wrap it, store as "this". */
    ShapeRef *shape = new ShapeRef(router, *poly);
    shape->jsref = PTR_WRAP(args.This(), shape, PTR_SHAPEREF,
                            avoid_ShapeRef_destruct);
    log_msg("Created ShapeRef %p (router=%p jsref=%p)", shape, router,
            shape->jsref);
    return scope.Close(args.This());
}

/*
 * Shape.id() -> IntegerID
 *
 * Return shape ID (numeric).
 */
static Handle<Value> avoid_ShapeRef_id(const Arguments& args)
{
    HandleScope scope;
    JS_ARG_RANGE(0, 0);

    PTR_EXTRACT(ShapeRef, shape, args.This(), PTR_SHAPEREF);
    return scope.Close(Integer::NewFromUnsigned(shape->id()));
}

/*
 * Shape.morph(arrayOfPoints) -> this
 *
 * Morph shape into another Polygon shape.
 */
static Handle<Value> avoid_ShapeRef_morph(const Arguments& args)
{
    HandleScope scope;
    JS_ARG_RANGE(1, 1);
    JS_ARGCHECK_ARRAY(0);

    /* Create empty polygon. */
    Local<Array> points = Local<Array>::Cast(args[0]);
    unsigned n = points->Length();
    Polygon poly(n);

    /* Iterate over supplied points and add them to polygon. */
    for (unsigned i = 0; i < n; i++) {
        double x, y;
        JS_EXTRACT_VECTOR(points->Get(i), x, y);
        poly.setPoint(i, Point(x, y));
    }

    /* Call moveShape() on router instance. */
    PTR_EXTRACT(ShapeRef, shape, args.This(), PTR_SHAPEREF);
    shape->router()->moveShape(shape, poly);

    return scope.Close(args.This());
}

/*
 * Shape.morph(arrayOfPoints) -> this
 *
 * Move shape by relative distance.
 */
static Handle<Value> avoid_ShapeRef_move(const Arguments& args)
{
    HandleScope scope;
    JS_ARG_RANGE(1, 1);
    JS_ARGCHECK_OBJECT(0);

    /* Extract offset vector. */
    double x, y;
    JS_EXTRACT_VECTOR(args[0], x, y);

    /* Call moveShape() on router instance. */
    PTR_EXTRACT(ShapeRef, shape, args.This(), PTR_SHAPEREF);
    shape->router()->moveShape(shape, x, y);

    return scope.Close(args.This());
}

/*
 * Shape.Pin(pinClassName, xOffset=0.5, yOffset=0.5, iOffset=0.0, dir=none)
 *
 * Add pin to shape.
 */
static Handle<Value> avoid_ShapeRef_Pin(const Arguments& args)
{
    HandleScope scope;
    JS_ARG_RANGE(1, 5);

    /* Produce pin. */
    Handle<Value> argv[6] = { args.This(), args[0], args[1], args[2], args[3],
                              args[4] };
    Local<Object> pin = avoid_Pin_factory->NewInstance(6, argv);
    return scope.Close(pin);
}

/* Javascript function that creates new ShapeRef instances. */
static Persistent<Function> avoid_ShapeRef_factory;

/* Object setup: create factory function. */
static void avoid_ShapeRef_setup(void)
{
    Local<FunctionTemplate> tpl = ptr_template("ShapeRef", avoid_ShapeRef_new);

    /* Add methods. */
    Local<ObjectTemplate> ptt = tpl->PrototypeTemplate();
    JS_SET_METHOD(ptt, "id", avoid_ShapeRef_id);
    JS_SET_METHOD(ptt, "morph", avoid_ShapeRef_morph);
    JS_SET_METHOD(ptt, "move", avoid_ShapeRef_move);
    JS_SET_METHOD(ptt, "Pin", avoid_ShapeRef_Pin);

    avoid_ShapeRef_factory = Persistent<Function>::New(tpl->GetFunction());
}

#endif /* JSAVOID_SHAPEREF_H */
