#ifndef JSAVOID_UTIL_H
#define JSAVOID_UTIL_H

/* Using _snprintf (MSVC++ version of snprintf) may be unsafe but who cares. */
#ifdef _MSC_VER
    #ifndef snprintf
        #define snprintf _snprintf
    #endif
#endif

/* Shorthand. */
typedef Handle<Value> (*jsfunc)(const Arguments&);

/* Source location. */
#define STRINGIFY(x) #x
#define TOSTRING(x) STRINGIFY(x)
#define SOURCE_LOC __FILE__ ":" TOSTRING(__LINE__)

/* Produce error message and return from Javascript function. */
#define JS_SCOPE_ERROR(msg) \
do { \
    ThrowException(Exception::Error(String::New(msg))); \
    return scope.Close(Undefined()); \
} while (0)

/* Return pointer to static buffer with the complete error message in it. */
static const char *c_msg(const char *fmt, ...)
{
    static char buf[512];
    va_list ap;
    va_start(ap, fmt);
    vsnprintf(buf, sizeof(buf), fmt, ap);
    va_end(ap);
    return buf;
}

/* Make sure the number of arguments falls within a certain range. */
#define JS_ARG_RANGE(lower, upper) \
    if (args.Length() < (lower) || args.Length() > (upper)) { \
        JS_SCOPE_ERROR(c_msg("Wrong number of arguments in " SOURCE_LOC \
                             "\n  Expected (%d <= numargs <= %d), got %d.", \
                             (lower), (upper), args.Length())); \
    }

/* Assert a condition. Throw JS scope error if the assertion fails. */
#define JS_ASSERT(cond, fmt) \
    if (!(cond)) { \
        JS_SCOPE_ERROR(c_msg("[C++] Assertion (%s) failed in " SOURCE_LOC " " fmt, #cond)); \
    }

/* Argument type checks. */
#define JS_ARGCHECK(index, Type) \
    if (!args[(index)]->Is##Type()) { \
        JS_SCOPE_ERROR(c_msg("Expected %s as argument (%d) at " SOURCE_LOC, #Type, (index))); \
    }
#define JS_ARGCHECK_ARRAY(index) JS_ARGCHECK((index), Array)
#define JS_ARGCHECK_OBJECT(index) JS_ARGCHECK((index), Object)
#define JS_ARGCHECK_NUMBER(index) JS_ARGCHECK((index), Number)
#define JS_ARGCHECK_INT32(index) JS_ARGCHECK((index), Int32)
#define JS_ARGCHECK_EXTERNAL(index) JS_ARGCHECK((index), External)
#define JS_ARGCHECK_STRING(index) JS_ARGCHECK((index), String)
#define JS_ARGCHECK_FUNCTION(index) JS_ARGCHECK((index), Function)
#define JS_ARGCHECK_BOOLEAN(index) JS_ARGCHECK((index), Boolean)

#define JS_DEFARG_NUMBER(index, var_name, default_value) \
    double var_name = (default_value); \
    if (!args[(index)]->IsUndefined()) { \
        JS_ARGCHECK_NUMBER(index); \
        var_name = args[(index)]->NumberValue(); \
    }
#define JS_DEFARG_INT32(index, var_name, default_value) \
    int32_t var_name = (default_value); \
    if (!args[(index)]->IsUndefined()) { \
        JS_ARGCHECK_INT32(index); \
        var_name = args[(index)]->Int32Value(); \
    }
#define JS_DEFARG_BOOL(index, var_name, default_value) \
    bool var_name = (default_value); \
    if (!args[(index)]->IsUndefined()) { \
        var_name = args[(index)]->BooleanValue(); \
    }

/* Add objects to template. */
#define JS_SET_METHOD(tpl, name, method) \
    tpl->Set(String::NewSymbol(name), \
             FunctionTemplate::New(method)->GetFunction())
#define JS_SET_INTEGER(tpl, name, value) \
    tpl->Set(String::NewSymbol(name), Integer::New(value))

/*
 * Extract coordinates from vector object {x: number, y: number}.
 */
#define JS_EXTRACT_VECTOR(value, x, y) \
do { \
    JS_ASSERT((value)->IsObject(), "Vector must be an Object or an Array."); \
    Local<Object> obj = (value)->ToObject(); \
    if (obj->IsArray()) { \
        Local<Array> a = Local<Array>::Cast(obj); \
        JS_ASSERT(a->Length() == 2, "Expected array with two elements."); \
        JS_ASSERT(a->Get(0)->IsNumber(), "1st element is not a number."); \
        JS_ASSERT(a->Get(1)->IsNumber(), "2nd element is not a number."); \
        (x) = a->Get(0)->NumberValue(); \
        (y) = a->Get(1)->NumberValue(); \
    } else { \
        Local<Value> h_x = obj->Get(String::NewSymbol("x")); \
        Local<Value> h_y = obj->Get(String::NewSymbol("y")); \
        JS_ASSERT(h_x->IsNumber(), "Vector x coordinate is not a number."); \
        JS_ASSERT(h_y->IsNumber(), "Vector y coordinate is not a number."); \
        (x) = h_x->NumberValue(); \
        (y) = h_y->NumberValue(); \
    } \
} while (0)

/*
 * Extract pinned connection endpoint from object
 *      {shape: ShapeRef, pins: pinClassName}
 */
#define JS_EXTRACT_PINNED_CONNEND(obj, shape, pinclass_id) \
do { \
    Local<Value> h_shape = (obj)->Get(String::NewSymbol("shape")); \
    Local<Value> h_pins = (obj)->Get(String::NewSymbol("pins")); \
    JS_ASSERT(h_shape->IsObject(), "\"shape\" member for endpoint not an object."); \
    JS_ASSERT(h_pins->IsString(), "\"pins\" member for endpoint not a string."); \
    \
    /* Extract shape pointer. */ \
    Pointer *s_ptr = PTR_UNWRAP(h_shape->ToObject()); \
    PTR_CHECK(s_ptr, PTR_SHAPEREF); \
    shape = (ShapeRef *)s_ptr->p; \
    \
    /* Get pin-class ID. */ \
    String::Utf8Value pinClassName(h_pins->ToString()); \
    pinclass_id = pin_getClassID(*pinClassName); \
} while (0)

#endif /* JSAVOID_UTIL_H */
