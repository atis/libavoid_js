#ifndef JSAVOID_PIN_H
#define JSAVOID_PIN_H

#include "uthash.h"

/*
 * uthash lookup structure. Maps pin class names (strings) to pin class IDs as
 * used by libavoid.
 */
typedef struct {
    char     name[128];   /* Pin class name = hash key. */
    unsigned id;          /* Pin class ID. */
    UT_hash_handle hh;    /* Make it hashable. */
} PinClass_Entry;

static unsigned PinClass_last_id;   /* Counter for generating pin-class IDs. */
static PinClass_Entry *pin_classes;

static unsigned pin_getClassID(const char *class_name)
{
    assert(class_name != NULL);

    /* See if pin class with specified name already exists. */
    PinClass_Entry *pc;
    HASH_FIND_STR(pin_classes, class_name, pc);
    if (pc != NULL)
        return pc->id;

    /* Create a new pin-class entry. */
    pc = (PinClass_Entry *)malloc(sizeof(*pc));
    assert(strlen(class_name) <= sizeof(pc->name));
    snprintf(pc->name, sizeof(pc->name), "%s", class_name);
    pc->id = ++PinClass_last_id;
    HASH_ADD_STR(pin_classes, name, pc);
    log_msg("Created pin-class \"%s\" (id=%d)", pc->name, pc->id);
    return pc->id;
}

/*
 * Pin.setExclusive(boolean) -> this
 *
 * Sets whether the pin is exclusive, i.e., only one connector can attach to it.
 */
static Handle<Value> avoid_Pin_setExclusive(const Arguments& args)
{
    HandleScope scope;
    JS_ARG_RANGE(1, 1);
    JS_ARGCHECK_BOOLEAN(0);

    /* Call setExclusive(). */
    PTR_EXTRACT(ShapeConnectionPin, pin, args.This(), PTR_PIN);
    pin->setExclusive(args[0]->BooleanValue());
    
    return scope.Close(args.This());
}

/* JS reference destructor (used as argument to Pointer::Pointer()). */
static void avoid_Pin_destruct(void *p)
{
    assert(p != NULL);
    ((ShapeConnectionPin *)p)->jsref = NULL;
}

/* Create Pin. */
static Handle<Value> avoid_Pin_new(const Arguments& args)
{
    HandleScope scope;
    JS_ARG_RANGE(6, 6);
    JS_ARGCHECK_OBJECT(0);
    JS_ARGCHECK_STRING(1);

    /* Get pin-class ID from its name. */
    String::Utf8Value class_name(args[1]->ToString());
    unsigned class_id = pin_getClassID(*class_name);

    /* Extract all the number arguments. */
    JS_DEFARG_NUMBER(2, xOffset, 0.5);
    JS_DEFARG_NUMBER(3, yOffset, 0.5);
    JS_DEFARG_NUMBER(4, iOffset, 0.0);
    JS_DEFARG_INT32(5, dir, ConnDirNone);

    /* Create Pin object, return its wrapped form. */
    PTR_EXTRACT(ShapeRef, shape, args[0]->ToObject(), PTR_SHAPEREF);
    ShapeConnectionPin *pin = new ShapeConnectionPin(shape, class_id, xOffset,
                                                     yOffset, iOffset, dir);
    pin->jsref = PTR_WRAP(args.This(), pin, PTR_PIN, avoid_Pin_destruct);
    log_msg("Created Pin %p (class=%d shape=%p jsref=%p)", pin, class_id,
            shape, pin->jsref);
    return scope.Close(args.This());
}

/* Javascript function that creates new Pin instances. */
static Persistent<Function> avoid_Pin_factory;

/* Object setup: create factory function. */
static void avoid_Pin_setup(void)
{
    Local<FunctionTemplate> tpl = ptr_template("Pin", avoid_Pin_new);

    /* Add methods. */
    Local<ObjectTemplate> ptt = tpl->PrototypeTemplate();
    JS_SET_METHOD(ptt, "setExclusive", avoid_Pin_setExclusive);

    avoid_Pin_factory = Persistent<Function>::New(tpl->GetFunction());
}

#endif /* JSAVOID_PIN_H */
