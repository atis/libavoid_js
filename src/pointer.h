#ifndef JSAVOID_POINTER_H
#define JSAVOID_POINTER_H

enum {
    PTR_NONE = 10000,
    PTR_ROUTER,
    PTR_SHAPEREF,
    PTR_CONNREF,
    PTR_PIN,
    PTR_LAST
};

static const char *ptr_type_name(int type_id)
{
    switch (type_id) {
        case PTR_NONE: return "PTR_NONE -- object has probably been deleted";
        case PTR_ROUTER: return "Router";
        case PTR_SHAPEREF: return "Shape";
        case PTR_CONNREF: return "Connector";
        case PTR_PIN: return "Pin";
    }
    return "Unknown";
}

/* Destructor function type definition. */
typedef void (*ptr_dealloc_fn)(void *);

/*
 * Wrap a pointer in a struct that inherits from ObjectWrap, so that we can give
 * the pointer to Javascript.
 */
struct Pointer : node::ObjectWrap {
    /* Arguments: a pointer to wrap and (optionally) its deallocator func. */
    Pointer(void *_p, int _type_id, ptr_dealloc_fn fn) {
        assert(_p != NULL && _type_id > 0);
        p = _p;
        type_id = _type_id;
        destructor = fn;
    }
    ~Pointer() {
        if (p == NULL) {
            assert(type_id == PTR_NONE && destructor == NULL);
            return;
        }
        if (destructor != NULL) /* Call deallocator func if it was set. */
            destructor(p);
        Invalidate();
    }

    /* Make node::ObjectWrap::Wrap protected function public. */
    Pointer *Wrap(Handle<Object> handle) {
        ObjectWrap::Wrap(handle);
        return this;
    }

    /* Discard wrapped pointer and associated values. */
    void Invalidate() {
        if (p != NULL) {
            p = NULL;
            destructor = NULL;
            type_id = PTR_NONE;
            pf_handle.Dispose();
            self.Dispose();
        }
    }

    void *p;        /* The most important member: the raw pointer. */
    int type_id;    /* For runtime verification. */
    ptr_dealloc_fn destructor;

    Persistent<Function> pf_handle;
    Persistent<Object> self;
};

/*
 * Registered with libavoid so that whenever a libavoid C++ object's destructor
 * is called (possibly internally by libavoid), its corresponding JS Pointer
 * object could be invalidated.
 */
void ptr_invalidate(void *p)
{
    Pointer *ptr = (Pointer *)p;
    assert(ptr != NULL && ptr->type_id > PTR_NONE && ptr->type_id < PTR_LAST);
    ptr->Invalidate();
}

/*
 * Wrap raw pointer (with optional dealloc function) inside a new Pointer
 * structure. This structure is then bound to handle.
 */
#define PTR_WRAP(handle, ptr, type_id, dealloc_fn) \
    ((new Pointer((ptr), (type_id), (dealloc_fn)))->Wrap(handle))

/* Unwrap an object of the given type. */
#define PTR_UNWRAP(obj) \
    (node::ObjectWrap::Unwrap<Pointer>(obj))

/* Verify if Pointer type_id matches expectations. */
#define PTR_CHECK(ptr, _type_id) \
    if (ptr->type_id != _type_id) { \
        JS_SCOPE_ERROR(c_msg("[PTR] Expected type_id=%d, got %d (%s)", _type_id, \
                             ptr->type_id, ptr_type_name(ptr->type_id))); \
    }

/* Extract pointer value from handle; declare Type pointer and assign to it. */
#define PTR_EXTRACT(Type, var_name, handle, type_id) \
    Pointer *ptr_##var_name = PTR_UNWRAP(handle); \
    PTR_CHECK(ptr_##var_name, (type_id)); \
    Type *var_name = (Type *)ptr_##var_name->p;

/* Return wrapped pointer memory address as string. */
static Handle<Value> ptr_addr(const Arguments& args)
{
    HandleScope scope;
    Pointer *ptr = PTR_UNWRAP(args.This());
    JS_ASSERT(ptr->type_id > PTR_NONE && ptr->type_id < PTR_LAST,
              "Pointer structure does not have a valid type ID.");
    char tmp[32];
    snprintf(tmp, sizeof(tmp), "%p", ptr->p);
    return scope.Close(String::New(tmp));
}

/* Used by console.log() to get string representation of Pointer objects. */
static Handle<Value> ptr_inspect(const Arguments& args)
{
    HandleScope scope;
    Pointer *ptr = PTR_UNWRAP(args.This());
    char tmp[256];
    snprintf(tmp, sizeof(tmp), "(%s at %p)",
             ptr_type_name(ptr->type_id), ptr->p);
    return scope.Close(String::New(tmp));
}

/*
 * Return a function template for creating objects with the given constructor
 * function.
 */
static Local<FunctionTemplate>
ptr_template(const char *class_name, jsfunc alloc_fn)
{
    Local<FunctionTemplate> tpl = FunctionTemplate::New(alloc_fn);
    tpl->SetClassName(String::NewSymbol(class_name));
    tpl->InstanceTemplate()->SetInternalFieldCount(1);

    /* Add addr() and inspect() member functions to prototype. */
    Local<ObjectTemplate> ptt = tpl->PrototypeTemplate();
    JS_SET_METHOD(ptt, "addr", ptr_addr);
    JS_SET_METHOD(ptt, "inspect", ptr_inspect);
    return tpl;
}

#endif  /* JSAVOID_POINTER_H */
