#include <node.h>
#include <v8.h>
#include <libavoid/libavoid.h>

using namespace v8;
using namespace Avoid;

extern "C" {
    #include "log.h"
}
#include "util.h"
#include "pointer.h"
#include "Pin.h"
#include "ShapeRef.h"
#include "ConnRef.h"
#include "Router.h"

/* Return libavoid_js.node version string. */
static Handle<Value> version(const Arguments& args)
{
    HandleScope scope;
    JS_ARG_RANGE(0, 0);
    return scope.Close(String::New("1.4"));
}

/* Delete any libavoid object. */
static Handle<Value> avoid_destroy(const Arguments& args)
{
    HandleScope scope;
    JS_ARG_RANGE(1, 1);
    JS_ARGCHECK_OBJECT(0);

    Pointer *ptr = PTR_UNWRAP(args[0]->ToObject());
    switch (ptr->type_id) {
        case PTR_ROUTER: {
            avoid_Router_destruct(ptr->p);
            break;
        }
        case PTR_CONNREF: {
            ConnRef *conn = (ConnRef *)ptr->p;
            log_msg("Deleting ConnRef instance %p (router=%p jsref=%p)", conn,
                    conn->router(), ptr);
            conn->router()->deleteConnector(conn);
            break;
        }
        case PTR_SHAPEREF: {
            ShapeRef *shape = (ShapeRef *)ptr->p;
            log_msg("Deleting ShapeRef instance %p (router=%p jsref=%p)", shape,
                    shape->router(), ptr);
            shape->router()->deleteShape(shape);
            break;
        }
        default: {
            char tmp[256];
            snprintf(tmp, sizeof(tmp), "Invalid argument to destroy(): %s, type_id=%d.",
                     ptr_type_name(ptr->type_id), ptr->type_id);
            JS_SCOPE_ERROR(tmp);
        }
    }
    ptr->Invalidate();
    return scope.Close(Undefined());
}

/*
 * Node addon module initialization function.
 */
void init(Handle<Object> target)
{
    log_open(NULL);

    /* Register JS reference invalidator function with libavoid. */
    Avoid::register_jsref_invalidator(ptr_invalidate);

    /* Set up JS object constructors. */
    avoid_Router_setup();
    avoid_ShapeRef_setup();
    avoid_ConnRef_setup();
    avoid_Pin_setup();

    JS_SET_METHOD(target, "version", version);
    JS_SET_METHOD(target, "Router", avoid_Router);
    JS_SET_METHOD(target, "destroy", avoid_destroy);

    /* Visibility direction values from connend.h */
    JS_SET_INTEGER(target, "DirNone", ConnDirNone); /* 0 */
    JS_SET_INTEGER(target, "Up", ConnDirUp);        /* 1 */
    JS_SET_INTEGER(target, "Down", ConnDirDown);    /* 2 */
    JS_SET_INTEGER(target, "Left", ConnDirLeft);    /* 4 */
    JS_SET_INTEGER(target, "Right", ConnDirRight);  /* 8 */
    JS_SET_INTEGER(target, "DirAll", ConnDirAll);   /* 15 */

    JS_SET_INTEGER(target, "PolyLineRouting", PolyLineRouting);
    JS_SET_INTEGER(target, "OrthogonalRouting", OrthogonalRouting);
}
NODE_MODULE(libavoid_js, init)  /* dllexport */
