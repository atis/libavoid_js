# Please make sure this file uses Unix-style line endings (\n).
{
    "targets": [
        {
            "target_name": "libavoid",
            "type": "static_library",
            "include_dirs": [ "." ],
            "sources": [
                "libavoid/actioninfo.cpp",
                "libavoid/connectionpin.cpp",
                "libavoid/connector.cpp",
                "libavoid/connend.cpp",
                "libavoid/geometry.cpp",
                "libavoid/geomtypes.cpp",
                "libavoid/graph.cpp",
                "libavoid/graph.cpp",
                "libavoid/hyperedge.cpp",
                "libavoid/junction.cpp",
                "libavoid/makepath.cpp",
                "libavoid/mtst.cpp",
                "libavoid/obstacle.cpp",
                "libavoid/orthogonal.cpp",
                "libavoid/router.cpp",
                "libavoid/scanline.cpp",
                "libavoid/shape.cpp",
                "libavoid/timer.cpp",
                "libavoid/vertices.cpp",
                "libavoid/viscluster.cpp",
                "libavoid/visibility.cpp",
                "libavoid/vpsc.cpp"
            ],
            "conditions": [
                ["OS=='mac'", {
                    "cflags_cc!": [ "-fno-rtti", "-fno-exceptions" ],
                    "xcode_settings": {
                        "GCC_ENABLE_CPP_RTTI": "YES",
                        "GCC_ENABLE_CPP_EXCEPTIONS": "YES"
                    }
                }]
            ]
        }
    ]
}
